/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h
 * @brief File with function prototypes
 *
 * This file contains 7 function prototypes.
 *
 * @author Peneac Malvin
 * @date 29.08.2021
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/**
 * @brief Prints statistics for an array
 *
 * This function uses all the other functions that are implemented and shows all the data in a formated order.
 *
 * @param array pointer to the array
 * @param length length of the array
 *
 */
void print_statistics(unsigned char* array, unsigned int length);

/**
 * @brief Prints a char array
 *
 * This function prints out every char from a char array, separated by a space.
 *
 * @param array pointer to the array
 * @param length length of the array
 *
 */
void print_array(unsigned char* array, unsigned int length);

/**
 * @brief Returns the median value of an array
 *
 * This functions returns the middle value of an array.
 *
 * @param array pointer to the array
 * @param length length of the array
 *
 * @return The median of the array
 */
unsigned char find_median(unsigned char* array, unsigned int length);

/**
 * @brief Returns the mean value of an array
 *
 * This function returns the average value of an array.
 *
 * @param array pointer to the array
 * @param length length of the array
 *
 * @return The mean of the array
 */
unsigned char find_mean(unsigned char* array, unsigned int length);

/**
 * @brief Return the maximum value of an array
 *
 * This function returns the highest value of an array.
 *
 * @param array pointer to the array
 * @param length length of the array
 *
 * @return The maximum value of the array
 */
unsigned char find_maximum(unsigned char* array, unsigned int length);

/**
 * @brief Returns the minimum value of an array
 *
 * This function returns the lowest value of an array.
 *
 * @param array pointer to the array
 * @param length length of the array
 *
 * @return The minimum value of the array
 */
unsigned char find_minimum(unsigned char* array, unsigned int length);

/**
 * @brief This function sorts in descending order an array
 *
 * This function iterates through all the elements and sorts them in descending order without returning a new array.
 *
 * @param array pointer to the array
 * @param length length of the array
 */
void sort_array(unsigned char* array, unsigned int length);

#endif /* __STATS_H__ */