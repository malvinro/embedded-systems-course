/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c
 * @brief File with function implementations
 *
 * This file contains 7 function implementations and the main function.
 *
 * @author Peneac Malvin
 * @date 29.08.2021
 *
 */

#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};
  print_statistics(test, SIZE);
}

void print_statistics(unsigned char* array, unsigned int length)
{
    printf(" 0.Printing array statistics...\n");
    printf(" 1.Initial array (print_array): \n");
    print_array(array, length); 
    
    printf(" 2. Minimum value (find_minimum): %u\n", find_minimum(array, length));

    printf(" 3. Maximum value (find_maximum): %u\n", find_maximum(array, length));

    printf(" 4. Mean value (find_mean): %u\n", find_mean(array, length));

    printf(" 5. Sorted array (sort_array): \n");
    sort_array(array, length);
    print_array(array, length);

    printf(" 6. Median value: %u\n", find_median(array, length));
}

void print_array(unsigned char* array, unsigned int length)
{
    for(int i = 0; i < length; i++)
    {
        printf("%u ", array[i]);
    }
    printf("\n");
}

unsigned char find_median(unsigned char* array, unsigned int length)
{
    sort_array(array, length);

    if(length % 2 != 0)
        return array[length / 2];

    return (unsigned char)(array[(length - 1) / 2] + array[length / 2]) / 2;
}

unsigned char find_mean(unsigned char* array, unsigned int length)
{
    int total = 0;
    for(int i = 0; i < length; i++)
        total += array[i];
    return (unsigned char)(total / length);
}

unsigned char find_maximum(unsigned char* array, unsigned int length)
{
    unsigned char maxChar = array[0];
    for(int i = 1; i < length; i++)
    {
        if(maxChar < array[i])
            maxChar = array[i];
    }
    return maxChar;
}

unsigned char find_minimum(unsigned char* array, unsigned int length)
{
    unsigned char minChar = array[0];
    for(int i = 1; i < length; i++)
    {
        if(minChar > array[i])
            minChar = array[i];
    }
    return minChar;
}

void sort_array(unsigned char* array, unsigned int length)
{
    int i, key, j;
    for (i = 1; i < length; i++) {
        key = array[i];
        j = i - 1;

        while (j >= 0 && array[j] < key) {
            array[j + 1] = array[j];
            j = j - 1;
        }

        array[j + 1] = key;
    }
}

